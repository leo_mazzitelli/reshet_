var fullP = false;
jQuery(document).ready(function(event){
	var isAnimating = false,
		newLocation = '';
		firstLoad = false;

	$('.menu-toggle').on('click', function(){
		console.log($('#menu-launcher').is(":checked"));
		if ($('#menu-launcher').is(":checked") == false){
			$('body').addClass('menu-open');
		}else{
			$('body').removeClass('menu-open');
		}
	});

	//trigger smooth transition from the actual page to the new one
	$('body').on('click', '[data-type="page-transition"]', function(event){

		event.preventDefault();
		//detect which page has been selected
		var newPage = $(this).attr('href');
		//if the page is not already being animated - trigger animation
		if( !isAnimating ) changePage(newPage, true);
		firstLoad = true;




	// Close menu when link clicked
		if($(this).hasClass("menu-link") || $(this).hasClass("brand-link")){
			 $('#menu-launcher').prop('checked', false);
		}
		if ($('#menu-launcher').is(":checked") == false){
			$('body').addClass('menu-open');
		}else{
			$('body').removeClass('menu-open');
		}
	});


  //trigger smooth transition from the actual page to the new one
	$('body').on('mouseover', '[data-hover="true"]', function(event){
	 $('body').addClass('dark-view');
  //inicio funciones hover imagenes carga página desde menu o enlace     
    $('#1_ibm').mouseover(function(event) {
	 $('#1_ibm_i').attr('src','/rehset/img/logos/Blanco/1_ibm.png');
     $('#c01').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#1_ibm_i').attr('src','/rehset/img/logos/Negro/1_ibm.png');
     $('#c01').removeClass('client-v2');
    });
    $('#2_cima').mouseover(function(event) {
	 $('#2_cima_i').attr('src','/rehset/img/logos/Blanco/2_cima.png');
     $('#c02').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#2_cima_i').attr('src','/rehset/img/logos/Negro/2_cima.png');
     $('#c02').removeClass('client-v2');
    });
    
    $('#3_fatro').mouseover(function(event) {
	 $('#3_fatro_i').attr('src','/rehset/img/logos/Blanco/3_fatro.png');
     $('#c03').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#3_fatro_i').attr('src','/rehset/img/logos/Negro/3_fatro.png');
     $('#c03').removeClass('client-v2');
    });
    
    $('#4_meller').mouseover(function(event) {
	 $('#4_meller_i').attr('src','/rehset/img/logos/Blanco/4_meller.png');
     $('#c04').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#4_meller_i').attr('src','/rehset/img/logos/Negro/4_meller.png');
     $('#c04').removeClass('client-v2');
    });
    
    $('#5_basf').mouseover(function(event) {
	 $('#5_basf_i').attr('src','/rehset/img/logos/Blanco/5_basf.png');
     $('#c05').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#5_basf_i').attr('src','/rehset/img/logos/Negro/5_basf.png');
     $('#c05').removeClass('client-v2');
    });
    
    $('#6_freixenet').mouseover(function(event) {
	 $('#6_freixenet_i').attr('src','/rehset/img/logos/Blanco/6_Freixenet.png');
     $('#c06').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#6_freixenet_i').attr('src','/rehset/img/logos/Negro/6_Freixenet.png');
     $('#c06').removeClass('client-v2');
    });
    
    $('#7_believe_a').mouseover(function(event) {
	 $('#7_believe_a_i').attr('src','/rehset/img/logos/Blanco/7_Believe_Athletics.png');
     $('#c07').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#7_believe_a_i').attr('src','/rehset/img/logos/Negro/7_Believe_Athletics.png');
     $('#c07').removeClass('client-v2');
    });
    
    $('#8_marybola').mouseover(function(event) {
	 $('#8_marybola_i').attr('src','/rehset/img/logos/Blanco/8_Marybola.png');
     $('#c08').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#8_marybola_i').attr('src','/rehset/img/logos/Negro/8_Marybola.png');
     $('#c08').removeClass('client-v2');
    });
    
    $('#9_micro').mouseover(function(event) {
	 $('#9_micro_i').attr('src','/rehset/img/logos/Blanco/9_Microsoft.png');
     $('#c09').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#9_micro_i').attr('src','/rehset/img/logos/Negro/9_Microsoft.png');
     $('#c09').removeClass('client-v2');
    });
    
    $('#10_buho').mouseover(function(event) {
	 $('#10_buho_i').attr('src','/rehset/img/logos/Blanco/10_Buho.png');
     $('#c10').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#10_buho_i').attr('src','/rehset/img/logos/Negro/10_Buho.png');
     $('#c10').removeClass('client-v2');
    });
    
    $('#11_ballena').mouseover(function(event) {
	 $('#11_ballena_i').attr('src','/rehset/img/logos/Blanco/11_La_ballena_alegre.png');
     $('#c11').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#11_ballena_i').attr('src','/rehset/img/logos/Negro/11_La_ballena_alegre.png');
     $('#c11').removeClass('client-v2');
    });
    
    $('#12_keapps').mouseover(function(event) {
	 $('#12_keapps_i').attr('src','/rehset/img/logos/Blanco/12_Keapps.png');
     $('#c12').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#12_keapps_i').attr('src','/rehset/img/logos/Negro/12_Keapps.png');
     $('#c12').removeClass('client-v2');
    });
    
    $('#13_barraquer').mouseover(function(event) {
	 $('#13_barraquer_i').attr('src','/rehset/img/logos/Blanco/13_Centro_Barraquer.png');
     $('#c13').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#13_barraquer_i').attr('src','/rehset/img/logos/Negro/13_Centro_Barraquer.png');
     $('#c13').removeClass('client-v2');
    });
    
    $('#14_merck').mouseover(function(event) {
	 $('#14_merck_i').attr('src','/rehset/img/logos/Blanco/14_Merck.png');
     $('#c14').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#14_merck_i').attr('src','/rehset/img/logos/Negro/14_Merck.png');
     $('#c14').removeClass('client-v2');
    });

    $('#15_foment').mouseover(function(event) {
	 $('#15_foment_i').attr('src','/rehset/img/logos/Blanco/15_Foment.png');
     $('#c15').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#15_foment_i').attr('src','/rehset/img/logos/Negro/15_Foment.png');
     $('#c15').removeClass('client-v2');
    });
    
    $('#17_drops').mouseover(function(event) {
	 $('#17_drops_i').attr('src','/rehset/img/logos/Blanco/17_Drops_and_you.png');
     $('#c17').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#17_drops_i').attr('src','/rehset/img/logos/Negro/17_Drops_and_you.png');
     $('#c17').removeClass('client-v2');
    });
    
    $('#18_insta').mouseover(function(event) {
	 $('#18_insta_i').attr('src','/rehset/img/logos/Blanco/18_Instasent.png');
     $('#c18').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#18_insta_i').attr('src','/rehset/img/logos/Negro/18_Instasent.png');
     $('#c18').removeClass('client-v2');
    });
    
    $('#19_sage').mouseover(function(event) {
	 $('#19_sage_i').attr('src','/rehset/img/logos/Blanco/19_sage.png');
     $('#c19').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#19_sage_i').attr('src','/rehset/img/logos/Negro/19_sage.png');
     $('#c19').removeClass('client-v2');
    });
    
    $('#20_natural').mouseover(function(event) {
	 $('#20_natural_i').attr('src','/rehset/img/logos/Blanco/20_Natural_Optics.png');
     $('#c20').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#20_natural_i').attr('src','/rehset/img/logos/Negro/20_Natural_Optics.png');
     $('#c20').removeClass('client-v2');
    });
    
    $('#21_fch').mouseover(function(event) {
	 $('#21_fch_i').attr('src','/rehset/img/logos/Blanco/21_Federacio_Catalana_dhipica.png');
     $('#c21').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#21_fch_i').attr('src','/rehset/img/logos/Negro/21_Federacio_Catalana_dhipica.png');
     $('#c21').removeClass('client-v2');
    });
    
    $('#22_magna').mouseover(function(event) {
	 $('#22_magna_i').attr('src','/rehset/img/logos/Blanco/22_La_Magna.png');
     $('#c22').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#22_magna_i').attr('src','/rehset/img/logos/Negro/22_La_Magna.png');
     $('#c22').removeClass('client-v2');
    });
    
    $('#23_letme').mouseover(function(event) {
	 $('#23_letme_i').attr('src','/rehset/img/logos/Blanco/23_Let_me_Space.png');
     $('#c23').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#23_letme_i').attr('src','/rehset/img/logos/Negro/23_Let_me_Space.png');
     $('#c23').removeClass('client-v2');
    });
    
    $('#24_radikal').mouseover(function(event) {
	 $('#24_radikal_i').attr('src','/rehset/img/logos/Blanco/24_Radikal_Vip.png');
     $('#c24').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#24_radikal_i').attr('src','/rehset/img/logos/Negro/24_Radikal_Vip.png');
     $('#c24').removeClass('client-v2');
    });
    
    $('#25_sahita').mouseover(function(event) {
	 $('#25_sahita_i').attr('src','/rehset/img/logos/Blanco/25_Sahita.png');
     $('#c25').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#25_sahita_i').attr('src','/rehset/img/logos/Negro/25_Sahita.png');
     $('#c25').removeClass('client-v2');
    });
    
    $('#26_nogapp').mouseover(function(event) {
	 $('#26_nogapp_i').attr('src','/rehset/img/logos/Blanco/26_Nogapp.png');
     $('#c26').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#26_nogapp_i').attr('src','/rehset/img/logos/Negro/26_Nogapp.png');
     $('#c26').removeClass('client-v2');
    });
    
    $('#27_1500').mouseover(function(event) {
	 $('#27_1500_i').attr('src','/rehset/img/logos/Blanco/27_1500_grados.png');
     $('#c27').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#27_1500_i').attr('src','/rehset/img/logos/Negro/27_1500_grados.png');
     $('#c27').removeClass('client-v2');
    });
    
    $('#28_pazo').mouseover(function(event) {
	 $('#28_pazo_i').attr('src','/rehset/img/logos/Blanco/28_Pazo_Pondal.png');
     $('#c28').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#28_pazo_i').attr('src','/rehset/img/logos/Negro/28_Pazo_Pondal.png');
     $('#c28').removeClass('client-v2');
    });
  //fin funciones hover imagenes      
	}).on('mouseout', '[data-hover="true"]', function(event){
	 $('body').removeClass('dark-view');
	}).on('click', '[data-hover="true"]', function(event){
	 $('body').removeClass('dark-view');
  //inicio control click sobre imagenes
     $('#1_ibm_i').attr('src','/rehset/img/logos/Negro/1_ibm.png');
     $('#c01').removeClass('client-v2');
     $('#2_cima_i').attr('src','/rehset/img/logos/Negro/2_cima.png');
     $('#c02').removeClass('client-v2');
     $('#3_fatro_i').attr('src','/rehset/img/logos/Negro/3_fatro.png');
     $('#c03').removeClass('client-v2');
     $('#4_meller_i').attr('src','/rehset/img/logos/Negro/4_meller.png');
     $('#c04').removeClass('client-v2');
     $('#5_basf_i').attr('src','/rehset/img/logos/Negro/5_basf.png');
     $('#c05').removeClass('client-v2');
     $('#6_freixenet_i').attr('src','/rehset/img/logos/Negro/6_Freixenet.png');
     $('#c06').removeClass('client-v2');
     $('#7_believe_a_i').attr('src','/rehset/img/logos/Negro/7_Believe_Athletics.png');
     $('#c07').removeClass('client-v2');
     $('#8_marybola_i').attr('src','/rehset/img/logos/Negro/8_Marybola.png');
     $('#c08').removeClass('client-v2');
     $('#9_micro_i').attr('src','/rehset/img/logos/Negro/9_Microsoft.png');
     $('#c09').removeClass('client-v2');
     $('#10_buho_i').attr('src','/rehset/img/logos/Negro/10_Buho.png');
     $('#c10').removeClass('client-v2');
     $('#11_ballena_i').attr('src','/rehset/img/logos/Negro/11_La_ballena_alegre.png');
     $('#c11').removeClass('client-v2');
     $('#12_keapps_i').attr('src','/rehset/img/logos/Negro/12_Keapps.png');
     $('#c12').removeClass('client-v2');
     $('#13_barraquer_i').attr('src','/rehset/img/logos/Negro/13_Centro_Barraquer.png');
     $('#c13').removeClass('client-v2');
     $('#14_merck_i').attr('src','/rehset/img/logos/Negro/14_Merck.png');
     $('#c14').removeClass('client-v2');
     $('#15_foment_i').attr('src','/rehset/img/logos/Negro/15_Foment.png');
     $('#c15').removeClass('client-v2');
     $('#17_drops_i').attr('src','/rehset/img/logos/Negro/17_Drops_and_you.png');
     $('#c17').removeClass('client-v2');
     $('#18_insta_i').attr('src','/rehset/img/logos/Negro/18_Instasent.png');
     $('#c18').removeClass('client-v2');
     $('#19_sage_i').attr('src','/rehset/img/logos/Negro/19_sage.png');
     $('#c19').removeClass('client-v2');
     $('#20_natural_i').attr('src','/rehset/img/logos/Negro/20_Natural_Optics.png');
     $('#c20').removeClass('client-v2');
     $('#21_fch_i').attr('src','/rehset/img/logos/Negro/21_Federacio_Catalana_dhipica.png');
     $('#c21').removeClass('client-v2');
     $('#22_magna_i').attr('src','/rehset/img/logos/Negro/22_La_Magna.png');
     $('#c22').removeClass('client-v2');
     $('#23_letme_i').attr('src','/rehset/img/logos/Negro/23_Let_me_Space.png');
     $('#c23').removeClass('client-v2');
     $('#24_radikal_i').attr('src','/rehset/img/logos/Negro/24_Radikal_Vip.png');
     $('#c24').removeClass('client-v2');
     $('#25_sahita_i').attr('src','/rehset/img/logos/Negro/25_Sahita.png');
     $('#c25').removeClass('client-v2');
     $('#26_nogapp_i').attr('src','/rehset/img/logos/Negro/26_Nogapp.png');
     $('#c26').removeClass('client-v2');
     $('#27_1500_i').attr('src','/rehset/img/logos/Negro/27_1500_grados.png');
     $('#c27').removeClass('client-v2');
     $('#28_pazo_i').attr('src','/rehset/img/logos/Negro/28_Pazo_Pondal.png');
     $('#c28').removeClass('client-v2');
 //fin control click sobre imagenes
	});
    

//inicialización de efectos hover recarga completa página    
    $('#1_ibm').mouseover(function(event) {
	 $('#1_ibm_i').attr('src','/rehset/img/logos/Blanco/1_ibm.png');
     $('#c01').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#1_ibm_i').attr('src','/rehset/img/logos/Negro/1_ibm.png');
     $('#c01').removeClass('client-v2');
    });
    
    $('#2_cima').mouseover(function(event) {
	 $('#2_cima_i').attr('src','/rehset/img/logos/Blanco/2_cima.png');
     $('#c02').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#2_cima_i').attr('src','/rehset/img/logos/Negro/2_cima.png');
     $('#c02').removeClass('client-v2');
    });
    
    $('#3_fatro').mouseover(function(event) {
	 $('#3_fatro_i').attr('src','/rehset/img/logos/Blanco/3_fatro.png');
     $('#c03').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#3_fatro_i').attr('src','/rehset/img/logos/Negro/3_fatro.png');
     $('#c03').removeClass('client-v2');
    });
    
    $('#4_meller').mouseover(function(event) {
	 $('#4_meller_i').attr('src','/rehset/img/logos/Blanco/4_meller.png');
     $('#c04').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#4_meller_i').attr('src','/rehset/img/logos/Negro/4_meller.png');
     $('#c04').removeClass('client-v2');
    });
    
    $('#5_basf').mouseover(function(event) {
	 $('#5_basf_i').attr('src','/rehset/img/logos/Blanco/5_basf.png');
     $('#c05').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#5_basf_i').attr('src','/rehset/img/logos/Negro/5_basf.png');
     $('#c05').removeClass('client-v2');
    });
    
    $('#6_freixenet').mouseover(function(event) {
	 $('#6_freixenet_i').attr('src','/rehset/img/logos/Blanco/6_Freixenet.png');
     $('#c06').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#6_freixenet_i').attr('src','/rehset/img/logos/Negro/6_Freixenet.png');
     $('#c06').removeClass('client-v2');
    });
    
    $('#7_believe_a').mouseover(function(event) {
	 $('#7_believe_a_i').attr('src','/rehset/img/logos/Blanco/7_Believe_Athletics.png');
     $('#c07').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#7_believe_a_i').attr('src','/rehset/img/logos/Negro/7_Believe_Athletics.png');
     $('#c07').removeClass('client-v2');
    });
    
    $('#8_marybola').mouseover(function(event) {
	 $('#8_marybola_i').attr('src','/rehset/img/logos/Blanco/8_Marybola.png');
     $('#c08').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#8_marybola_i').attr('src','/rehset/img/logos/Negro/8_Marybola.png');
     $('#c08').removeClass('client-v2');
    });
    
    $('#9_micro').mouseover(function(event) {
	 $('#9_micro_i').attr('src','/rehset/img/logos/Blanco/9_Microsoft.png');
     $('#c09').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#9_micro_i').attr('src','/rehset/img/logos/Negro/9_Microsoft.png');
     $('#c09').removeClass('client-v2');
    });
    
    $('#10_buho').mouseover(function(event) {
	 $('#10_buho_i').attr('src','/rehset/img/logos/Blanco/10_Buho.png');
     $('#c10').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#10_buho_i').attr('src','/rehset/img/logos/Negro/10_Buho.png');
     $('#c10').removeClass('client-v2');
    });
    
    $('#11_ballena').mouseover(function(event) {
	 $('#11_ballena_i').attr('src','/rehset/img/logos/Blanco/11_La_ballena_alegre.png');
     $('#c11').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#11_ballena_i').attr('src','/rehset/img/logos/Negro/11_La_ballena_alegre.png');
     $('#c11').removeClass('client-v2');
    });
    
    $('#12_keapps').mouseover(function(event) {
	 $('#12_keapps_i').attr('src','/rehset/img/logos/Blanco/12_Keapps.png');
     $('#c12').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#12_keapps_i').attr('src','/rehset/img/logos/Negro/12_Keapps.png');
     $('#c12').removeClass('client-v2');
    });
    
    $('#13_barraquer').mouseover(function(event) {
	 $('#13_barraquer_i').attr('src','/rehset/img/logos/Blanco/13_Centro_Barraquer.png');
     $('#c13').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#13_barraquer_i').attr('src','/rehset/img/logos/Negro/13_Centro_Barraquer.png');
     $('#c13').removeClass('client-v2');
    });
    
    $('#14_merck').mouseover(function(event) {
	 $('#14_merck_i').attr('src','/rehset/img/logos/Blanco/14_Merck.png');
     $('#c14').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#14_merck_i').attr('src','/rehset/img/logos/Negro/14_Merck.png');
     $('#c14').removeClass('client-v2');
    });

    $('#15_foment').mouseover(function(event) {
	 $('#15_foment_i').attr('src','/rehset/img/logos/Blanco/15_Foment.png');
     $('#c15').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#15_foment_i').attr('src','/rehset/img/logos/Negro/15_Foment.png');
     $('#c15').removeClass('client-v2');
    });
    
    $('#17_drops').mouseover(function(event) {
	 $('#17_drops_i').attr('src','/rehset/img/logos/Blanco/17_Drops_and_you.png');
     $('#c17').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#17_drops_i').attr('src','/rehset/img/logos/Negro/17_Drops_and_you.png');
     $('#c17').removeClass('client-v2');
    });
    
    $('#18_insta').mouseover(function(event) {
	 $('#18_insta_i').attr('src','/rehset/img/logos/Blanco/18_Instasent.png');
     $('#c18').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#18_insta_i').attr('src','/rehset/img/logos/Negro/18_Instasent.png');
     $('#c18').removeClass('client-v2');
    });
    
    $('#19_sage').mouseover(function(event) {
	 $('#19_sage_i').attr('src','/rehset/img/logos/Blanco/19_sage.png');
     $('#c19').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#19_sage_i').attr('src','/rehset/img/logos/Negro/19_sage.png');
     $('#c19').removeClass('client-v2');
    });
    
    $('#20_natural').mouseover(function(event) {
	 $('#20_natural_i').attr('src','/rehset/img/logos/Blanco/20_Natural_Optics.png');
     $('#c20').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#20_natural_i').attr('src','/rehset/img/logos/Negro/20_Natural_Optics.png');
     $('#c20').removeClass('client-v2');
    });
    
    $('#21_fch').mouseover(function(event) {
	 $('#21_fch_i').attr('src','/rehset/img/logos/Blanco/21_Federacio_Catalana_dhipica.png');
     $('#c21').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#21_fch_i').attr('src','/rehset/img/logos/Negro/21_Federacio_Catalana_dhipica.png');
     $('#c21').removeClass('client-v2');
    });
    
    $('#22_magna').mouseover(function(event) {
	 $('#22_magna_i').attr('src','/rehset/img/logos/Blanco/22_La_Magna.png');
     $('#c22').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#22_magna_i').attr('src','/rehset/img/logos/Negro/22_La_Magna.png');
     $('#c22').removeClass('client-v2');
    });
    
    $('#23_letme').mouseover(function(event) {
	 $('#23_letme_i').attr('src','/rehset/img/logos/Blanco/23_Let_me_Space.png');
     $('#c23').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#23_letme_i').attr('src','/rehset/img/logos/Negro/23_Let_me_Space.png');
     $('#c23').removeClass('client-v2');
    });
    
    $('#24_radikal').mouseover(function(event) {
	 $('#24_radikal_i').attr('src','/rehset/img/logos/Blanco/24_Radikal_Vip.png');
     $('#c24').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#24_radikal_i').attr('src','/rehset/img/logos/Negro/24_Radikal_Vip.png');
     $('#c24').removeClass('client-v2');
    });
    
    $('#25_sahita').mouseover(function(event) {
	 $('#25_sahita_i').attr('src','/rehset/img/logos/Blanco/25_Sahita.png');
     $('#c25').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#25_sahita_i').attr('src','/rehset/img/logos/Negro/25_Sahita.png');
     $('#c25').removeClass('client-v2');
    });
    
    $('#26_nogapp').mouseover(function(event) {
	 $('#26_nogapp_i').attr('src','/rehset/img/logos/Blanco/26_Nogapp.png');
     $('#c26').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#26_nogapp_i').attr('src','/rehset/img/logos/Negro/26_Nogapp.png');
     $('#c26').removeClass('client-v2');
    });
    
    $('#27_1500').mouseover(function(event) {
	 $('#27_1500_i').attr('src','/rehset/img/logos/Blanco/27_1500_grados.png');
     $('#c27').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#27_1500_i').attr('src','/rehset/img/logos/Negro/27_1500_grados.png');
     $('#c27').removeClass('client-v2');
    });
    
    $('#28_pazo').mouseover(function(event) {
	 $('#28_pazo_i').attr('src','/rehset/img/logos/Blanco/28_Pazo_Pondal.png');
     $('#c28').addClass('client-v2');
    }).mouseout(function(event) {
	 $('#28_pazo_i').attr('src','/rehset/img/logos/Negro/28_Pazo_Pondal.png');
     $('#c28').removeClass('client-v2');
    });
//fin inicialización efecto hover
    
	//detect the 'popstate' event - e.g. user clicking the back button
	$(window).on('popstate', function() {
		if( firstLoad ) {
			/*
			Safari emits a popstate event on page load - check if firstLoad is true before animating
			if it's false - the page has just been loaded
			*/
			var newPageArray = location.pathname.split('/'),
				//this is the url of the page to be loaded
				newPage = newPageArray[newPageArray.length - 1];

			if( !isAnimating  &&  newLocation != newPage ) changePage(newPage, false);
		}
		firstLoad = true;
	});

	function changePage(url, bool) {
		isAnimating = true;
		// trigger page animation
		console.log(url);
		// if(url== 'transformacion-digital.html'){
		//   console.log('yeh');
		//     setTimeout(function(){
		//       $.fn.fullpage.reBuild();
		//   }, 1000);
		// }
		$('body').addClass('page-is-changing');
		$('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			loadNewContent(url, bool);
			newLocation = url;
			$('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
		});
		//if browser doesn't support CSS transitions
		if( !transitionsSupported() ) {
			loadNewContent(url, bool);
			newLocation = url;
		}
	}

	function loadNewContent(url, bool) {
		url = ('' == url) ? 'index.html' : url;
		var newSection = 'cd-'+url.replace('.html', '');
		var section = $('<div class="cd-main-content '+newSection+'"></div>');

		section.load(url+' .cd-main-content > *', function(event){
            
			// load new content and replace <main> content with the new one
			$('main').html(section);
			//if browser doesn't support CSS transitions - dont wait for the end of transitions
			var delay = ( transitionsSupported() ) ? 1200 : 0;
			setTimeout(function(){
				//wait for the end of the transition on the loading bar before revealing the new content
				// ( section.hasClass('cd-about') ) ? $('body').addClass('cd-about') : $('body').removeClass('cd-about');
				$('body').removeClass().addClass(section.attr('class'));
				$('body').removeClass('page-is-changing');
				$('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
					isAnimating = false;
					$('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
				});

				if( !transitionsSupported() ) isAnimating = false;
			}, delay);

			if(url!=window.location && bool){
				//add the new page to the window.history
				//if the new page was triggered by a 'popstate' event, don't add it
				window.history.pushState({path: url},'',url);
			}
		});
	}

	function transitionsSupported() {
		return $('html').hasClass('csstransitions');
	}
});


