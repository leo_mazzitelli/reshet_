<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'C:\xampp\htdocs\rehset\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'rehset');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XFbj+S/6<]&v8oho<(:*K~0G,ER.OaNL6F6C{W%f[%Ic)*)SLwzr87kI7+7>1{,V');
define('SECURE_AUTH_KEY',  '|emLI%e 2BCeGHuK7s^;a-++0s-n[R!c%67A</t7rulHR6)RBO5@}Mp2G-y,;EW)');
define('LOGGED_IN_KEY',    '$8yPQ}2bvNXR ?^e-S*|OCGIXvI0b@&.M>H4?i4yO_$mZaL*NBEh-G@I^qixh`H8');
define('NONCE_KEY',        '+6iuz,wNF(7<2cAjT#4sBW|^/5ID)t8uF$!VUe},MAM=%tAo0kKeQZE0v.:bO]hH');
define('AUTH_SALT',        '4[*U`%YJ[M6vVx) =i@>t2IIOqt})8;Nt,^]:27nY>yyA-?0BkR xo|tA+dtK`j&');
define('SECURE_AUTH_SALT', 'p:2<%*?(bVcYAtYeDadU? /4W~&6 bA5]g8<KZW>h[pwNOwr!}W&x38bz)jv5CRv');
define('LOGGED_IN_SALT',   'Lu1i$l0>|PWnKlknQd9i@4H| (7,}Qk1$j3;Mu*Xn>u(2=sYfV,Isl.;oy7^#qnm');
define('NONCE_SALT',       'g]ZHWE0lh-Rh,u`qS.r9|5:T&kXN9%M)j)U+4;W@92CV_;=D`eD6&2K6 IB;sn,d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
