<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<div class="cd-cover-layer"></div>
<div class="cd-loading-bar"></div>
<script src="/rehset/js/main.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">
    
		<!--//--><![CDATA[//><!--
			var images = new Array()
			function preload() {
				for (i = 0; i < preload.arguments.length; i++) {
					images[i] = new Image()
					images[i].src = preload.arguments[i]
				}
			}
			preload(
				"/rehset/img/logos/Blanco/1_ibm.png",
				"/rehset/img/logos/Blanco/2_cima.png",
				"/rehset/img/logos/Blanco/3_fatro.png",
                "/rehset/img/logos/Blanco/4_meller.png",
				"/rehset/img/logos/Blanco/5_basf.png",
				"/rehset/img/logos/Blanco/6_Freixenet.png",
                "/rehset/img/logos/Blanco/7_Believe_Athletics.png",
				"/rehset/img/logos/Blanco/8_Marybola.png",
				"/rehset/img/logos/Blanco/9_Microsoft.png",
                "/rehset/img/logos/Blanco/10_Buho.png",
				"/rehset/img/logos/Blanco/11_La_ballena_alegre.png",
				"/rehset/img/logos/Blanco/12_Keapps.png",
                "/rehset/img/logos/Blanco/13_Centro_Barraquer.png",
				"/rehset/img/logos/Blanco/14_Merck.png",
				"/rehset/img/logos/Blanco/15_Foment.png",
                "/rehset/img/logos/Blanco/17_Drops_and_you.png",
				"/rehset/img/logos/Blanco/18_Instasent.png",
				"/rehset/img/logos/Blanco/19_sage.png",
                "/rehset/img/logos/Blanco/20_Natural_Optics.png",
				"/rehset/img/logos/Blanco/21_Federacio_Catalana_dhipica.png",
				"/rehset/img/logos/Blanco/22_La_Magna.png",
                "/rehset/img/logos/Blanco/23_Let_me_Space.png",
				"/rehset/img/logos/Blanco/24_Radikal_Vip.png",
				"/rehset/img/logos/Blanco/25_Sahita.png",
                "/rehset/img/logos/Blanco/26_Nogapp.png",
				"/rehset/img/logos/Blanco/27_1500_grados.png",
				"/rehset/img/logos/Blanco/28_Pazo_Pondal.png"
			)
		//--><!]]>

var mostrando=false;

function mostrar(id)
{

/*	var fondo=document.getElementById("entry-div-"+id)
	if(fondo!=undefined)
	fondo.style.opacity=0.5;
*/
		if(!mostrando)
		{
			$("#entry-div-"+id).animate({
						opacity: 0.5,
					}, 100, function() {
						mostrando=true
						//$(this).attr("style",$(this).attr("style")+" filter:grayscale(100%)")
					});
					//$('#img-fondo-'+i).attr("style",'opacity:0.5;z-index:0;-webkit-filter:grayscale(100%);   filter:grayscale(100%);');
				
		}
					
	
	return false;

}
function ocultar(id)
{
	/*var fondo=document.getElementById("entry-div-"+id)
	if(fondo!=undefined)
	fondo.style.opacity=0;
*/
if(mostrando)
{
			$("#entry-div-"+id).animate({
						opacity: 0,
					}, 100, function() {
						mostrando=false;
						//$(this).attr("style",$(this).attr("style")+" filter:grayscale(100%)")
					});
					//$('#img-fondo-'+i).attr("style",'opacity:0.5;z-index:0;-webkit-filter:grayscale(100%);   filter:grayscale(100%);');
				
}
					
	return false;
}
</script>
</body>
</html>
