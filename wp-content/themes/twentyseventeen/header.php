<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="/rehset/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="/rehset/assets/css/main.css"> <!-- Resource style -->

	<script src="/rehset/js/modernizr.js"></script> <!-- Modernizr -->
	<script src="/rehset/js/jquery-2.1.1.js"></script>


	<title>.Rehset</title>

	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92441939-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body <?php body_class(); ?>>
<header class="header">
	<input type="checkbox" id="menu-launcher"/>
	<nav style="z-index:100;">
		<label for="menu-launcher" class="menu-toggle menu-toggle__open">
			<svg width="130px" height="40px" viewBox="0 0 130 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="menu-icon">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<path d="M17.68,39 L26.512,12.984 L26.704,12.984 L26.704,39 L30.88,39 L30.88,8.76 L24.304,8.76 L16.24,33.384 L16.096,33.384 L7.6,8.76 L0.928,8.76 L0.928,39 L4.816,39 L4.816,12.984 L5.008,12.984 L14.176,39 L17.68,39 Z M76.4759999,8.76 L72.1559999,8.76 L72.1559999,39 L76.0439999,39 L76.0439999,14.712 L93.6119999,39 L96.9239999,39 L96.9239999,8.76 L93.0359999,8.76 L93.0359999,31.896 L76.4759999,8.76 Z M118.828,6.552 L123.484,0.504 L118.636,0.504 L116.044,6.552 L118.828,6.552 Z M117.436,39.576 C124.252,39.576 129.724,36.264 129.724,27.432 L129.724,8.76 L125.644,8.76 L125.644,27.528 C125.644,33.24 122.716,36.072 117.436,36.072 C112.156,36.072 109.324,33.24 109.324,27.48 L109.324,8.76 L105.196,8.76 L105.196,27.432 C105.196,36.264 110.62,39.576 117.436,39.576 Z" id="M-NÚ"></path>
					<g class="fix">
						<g id="Group-Copy" transform="translate(37.000000, 9.000000)">
							<g class="top-line">
								<polyline id="Rectangle" points="27.5 3.04518315e-15 27.5 4 0 4 0 0" ></polyline>
							</g>
							<g class="middle-line">
								<polyline id="Rectangle-Copy" points="27.5 13 27.5 17 0 17 0 13" ></polyline>
							</g>
							<g class="bottom-line">
								<polyline id="Rectangle-Copy-2" points="27.5 26 27.5 30 0 30 0 26" ></polyline>
							</g>
						</g>
					</g>
				</g>
			</svg>
		</label>
		<ul class="menu-list">
			<div class="menu">
				<li class="menu-item"><a href="/rehset/transformar-con-rehset/" data-type="page-transition" class="menu-link">Transformar con .Rehset</a></li>
				<li class="menu-item"><a href="/rehset/transformacion-digital/" data-type="page-transition" class="menu-link">Transformación Digital</a></li>
				<li class="menu-item"><a href="/rehset/nuestros-clientes/" data-type="page-transition" class="menu-link">Nuestros clientes</a></li>
				<li class="menu-item"><a href="/rehset/por-que-rehset/" data-type="page-transition" class="menu-link">Por qué .Rehset</a></li>
				<li class="menu-item"><a href="/rehset/rehset-jobs/" data-type="page-transition" class="menu-link">Trabaja en .Rehset</a></li>
				<li class="menu-item"><a href="/rehset/blog/" data-type="page-transition" class="menu-link">Nuestras Noticias</a></li>
			<!--	<li class="menu-item"><a href="nuestros-clientes_new2.html" data-type="page-transition" class="menu-link">.</a></li> -->
				

			</div>
			<div class="company-data">
				<ul class="address-list">
					<li>
						<span><strong>Barcelona</strong></span>
						<span>Balmes, 203 2o 2a</span>
						<span>08006 España</span>
						<a href="tel: +34938805740" class="tel-link">Tel. +34 93 880 57 40</a>
					</li>
					<li>
						<span><strong>Dublín</strong></span>
						<span>Digital Hub Roe Lane 8, Co.</span>
						<span>Dublin Ireland</span>
					</li>
					<li>
						<span><strong>Andorra</strong></span>
						<span>Torre Caldea 5a </span>
						<span>AD700 Escaldes - Engordany</span>
					</li>
					<li>
						<span><strong>Lecce</strong></span>
						<span>Via Corte Gaetano Stella 12</span>
						<span>73100 Italia</span>
					</li>
					<li>
						<span><a href="https://www.facebook.com/rehsetthinking">Facebook</a></span>
						<span><a href="https://twitter.com/@rehset_">Twitter</a></span>
						<span><a href="https://www.linkedin.com/company/rehset">LinkedIn</a></span>
					</li>
				</ul>

			</div>
		</ul>
		<div class="contact-link"><a href="mailto:hello@rehset.com">hello@rehset.com</a></div>
	</nav>
	<a href="/rehset/" data-type="page-transition" class="brand-link"><svg width="119px" height="91px" viewBox="0 0 119 91" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="brand-logo">
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<path d="M0.32,91 L25.088,91 L25.088,66.232 L0.32,66.232 L0.32,91 Z M118.544,91 C115.52,87.688 115.088,83.656 113.792,70.12 C112.784,59.608 107.312,54.28 97.952,52.408 C109.04,50.968 117.248,42.328 117.248,29.08 C117.248,11.368 105.584,0.28 82.112,0.28 L36.32,0.28 L36.32,91 L60.368,91 L60.368,60.904 L76.64,60.904 C84.848,60.904 89.312,63.208 90.032,73.576 C90.752,82.936 91.904,88.552 93.776,91 L118.544,91 Z M60.368,42.04 L60.368,19.864 L77.792,19.864 C88.16,19.864 93.344,23.752 93.344,31.096 C93.344,39.448 88.16,42.04 77.792,42.04 L60.368,42.04 Z" id=".R-Copy" fill="#000000"></path>
			</g>
		</svg>
	</a>

</header>