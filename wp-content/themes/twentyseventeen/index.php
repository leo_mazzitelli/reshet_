<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="cd-rehset cd-main-content">

<a style="text-decoration:none" href="/rehset/blog"><h1 style="margin-left:15%; margin-bottom:70px; font-size:50px;">NUESTRAS NOTICIAS</h1></a>

			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
				) );

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

	</div>

	<script>
var mostrando=false;

function mostrar(id)
{

/*	var fondo=document.getElementById("entry-div-"+id)
	if(fondo!=undefined)
	fondo.style.opacity=0.5;
*/
		if(!mostrando)
		{
			$("#entry-div-"+id).animate({
						opacity: 0.5,
					}, 100, function() {
						mostrando=true
						//$(this).attr("style",$(this).attr("style")+" filter:grayscale(100%)")
					});
					//$('#img-fondo-'+i).attr("style",'opacity:0.5;z-index:0;-webkit-filter:grayscale(100%);   filter:grayscale(100%);');
				
		}
					
	
	return false;

}
function ocultar(id)
{
	/*var fondo=document.getElementById("entry-div-"+id)
	if(fondo!=undefined)
	fondo.style.opacity=0;
*/
if(mostrando)
{
			$("#entry-div-"+id).animate({
						opacity: 0,
					}, 100, function() {
						mostrando=false;
						//$(this).attr("style",$(this).attr("style")+" filter:grayscale(100%)")
					});
					//$('#img-fondo-'+i).attr("style",'opacity:0.5;z-index:0;-webkit-filter:grayscale(100%);   filter:grayscale(100%);');
				
}
					
	return false;
}

</script>