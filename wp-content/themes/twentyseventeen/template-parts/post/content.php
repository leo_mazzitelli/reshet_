<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<?php if ( is_single() ) { ?>
<a style="text-decoration:none" href="/rehset/blog"><h1 style="margin-left:15%; margin-bottom:70px; font-size:50px;">NUESTRAS NOTICIAS</h1></a>
<?php } ?>
<div class="container-wrap" >
<article  id="post-<?php the_ID(); ?>"   <?php post_class(); ?>>
	<?php
		if ( is_sticky() && is_home() ) :
			echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
		endif;
	?>
	<header class="entry-header"<?php if ( !is_single() ){ ?>  onMouseout="ocultar(<?php the_ID(); ?>);" onMouseover="mostrar(<?php the_ID(); ?>);"<?php } ?> >
		<?php
			

			if ( is_single() ) {
				the_title( '<small style="opacity:0.6">'.get_the_category()[0]->name.'</small><h1 style="margin-top: 5px;width: 80%; font-size:42px;" class="entry-title">', '</h1>' );
			} elseif ( is_front_page() && is_home() ) {
				the_title( '<h1 class="entry-title"><a  style="text-decoration:none" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			} else {
				the_title( '<small style="opacity:0.6">'.get_the_category()[0]->name.'</small><h1 style="margin-top: 5px;width: 70%;" class="entry-title"><a style="text-decoration:none" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			}
		?>
	</header><!-- .entry-header -->

	

	<?php
		if ( 'post' === get_post_type() ) :
				echo '<div class="entry-meta" style="float:left;width:15%;margin-top:15px;">';
					if ( is_single() ) :
						twentyseventeen_posted_on();
					else :
						echo twentyseventeen_time_link();
						twentyseventeen_edit_link();
					endif;
				echo '</div><!-- .entry-meta -->';
			endif;
	?>

	<div class="entry-content"   onMouseout="ocultar(<?php the_ID(); ?>);" onMouseover="mostrar(<?php the_ID(); ?>);"  style="" >
			<?php if ( !is_single() ){ ?>
		<div class="entry-div"  onMouseout="ocultar(<?php the_ID(); ?>);" onMouseover="mostrar(<?php the_ID(); ?>);" id="entry-div-<?php the_ID(); ?>" style="background:url('<?php the_post_thumbnail_url( 'twentyseventeen-featured-image' ); ?>') center top; filter:greyscale(100%);">
		</div>
		<?php } ?>
				<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div>

<script> 
$('p').attr("style","font-size: 1.2rem;");

<?php if ( is_single() ) 
	{
		?>
$('.grey-over').mouseout(function(){
	$('.grey-over').attr("style","-webkit-filter:grayscale(100%);filter:grayscale(100%);");
})

$('.grey-over').mouseover(function(){
	$('.grey-over').attr("style","-webkit-filter:grayscale(0%);filter:grayscale(0%);");
})

var mostrando_parallax=false;
$('.vc_parallax').each(function(i, obj) {
	if($(obj).attr("data-vc-parallax-image")!="")
	{
		$(obj).html('<div class="entry-div-2" align="right" id="img-fondo-'+i+'" style="-webkit-transition:-webkit-filter 0.4s ease-in-out; -webkit-filter:grayscale(100%);grayscale(100%) blur(3px)"><img src="'+$(obj).attr("data-vc-parallax-image")+'" width="100%;" style="position: absolute;position: absolute;" /></div>'+$(obj).html());
	}
	
		setTimeout(function(){ 
			//le agrego funcionalidad
				$(obj).mouseout(function(){
					if(!mostrando_parallax)
					{
						$('#img-fondo-'+i).animate({
						opacity: 0.5,
						"z-index": 0,
						"-webkit-filter":"grayscale(100%)",
						filter:"grayscale(100%) blur(3px)",
						}, 200, function() {
							$(this).attr("style",$(this).attr("style")+" filter:grayscale(100%)")
							mostrando_parallax=true;
						});
					}
					
					//$('#img-fondo-'+i).attr("style",'opacity:0.5;z-index:0;-webkit-filter:grayscale(100%);   filter:grayscale(100%);');
				})

				$(obj).mouseover(function(){
					if(mostrando_parallax)
					{
						$('#img-fondo-'+i).animate({
						opacity: 1,
						"z-index": 1,
						"-webkit-filter":"grayscale(0%)",
						"box-shadow":"0 0 12px rgba(0,0,0,.7)",
						}, 200, function() {
							$(this).attr("style",$(this).attr("style")+" filter:grayscale(0%)")
							mostrando_parallax=false;
						});
					}
					
					//$('#img-fondo-'+i).attr("style",'opacity:1;z-index:1;box-shadow: 0 0 12px rgba(0,0,0,.7);');
				})
	 }, 500);

});



	
//data-vc-parallax-image

$('.transparent-over').mouseout(function(){
	$('.transparent-over').attr("style","opacity:0;z-index:0;");
})

$('.transparent-over').mouseover(function(){
	$('.transparent-over').attr("style","opacity:1;z-index:1;box-shadow: 0 0 12px rgba(0,0,0,.7);");
})


$('.wpb_wrapper').attr("style","margin-bottom: 50px;");

<?php }
?>
</script>

